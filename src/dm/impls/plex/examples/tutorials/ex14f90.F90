program ex14f90
   implicit none
#include <exodusII.inc>
#include <petsc-finclude/petsc.h90>

   PetscErrorCode                   :: ierr
   DM                               :: seqDM,distDM
   PetscBool                        :: flg,test_native=PETSC_FALSE,test_write=PETSC_FALSE,test_read=PETSC_FALSE,test_stdout=PETSC_FALSE,verbose=PETSC_FALSE
   PetscMPIInt                      :: numproc,rank
   PetscViewer                      :: hdf5Viewer
   PetscInt                         :: dim=2,sz,bs
   PetscInt                         :: numFields = 1
   PetscInt,dimension(:),pointer    :: numComp
   PetscInt,dimension(:),pointer    :: numDof 
   PetscInt,dimension(:),pointer    :: bcFields
   PetscInt                         :: numBC=0
   IS,dimension(:),pointer          :: bcPoints
   PetscSection                     :: seqSection,distSection
   Vec                              :: coord,distV,natV,readV
   PetscSF                          :: pointSF,global2naturalSF
   PetscReal,dimension(:),pointer   :: coords
   PetscReal                        :: norm
   character(len=512)               :: IOBuffer
   

   call PetscInitialize(PETSC_NULL_CHARACTER,ierr)
   call MPI_Comm_Rank(PETSC_COMM_WORLD,rank,ierr)
   call MPI_Comm_Size(PETSC_COMM_WORLD,numproc,ierr)

   call PetscOptionsGetBool(PETSC_NULL_CHARACTER,"-test_native",test_native,flg,ierr);CHKERRQ(ierr)
   call PetscOptionsGetBool(PETSC_NULL_CHARACTER,"-test_write",test_write,flg,ierr);CHKERRQ(ierr)
   call PetscOptionsGetBool(PETSC_NULL_CHARACTER,"-test_read",test_read,flg,ierr);CHKERRQ(ierr)
   call PetscOptionsGetBool(PETSC_NULL_CHARACTER,"-test_stdout",test_stdout,flg,ierr);CHKERRQ(ierr)
   call PetscOptionsGetBool(PETSC_NULL_CHARACTER,"-verbose",verbose,flg,ierr);CHKERRQ(ierr)
   call PetscOptionsGetInt(PETSC_NULL_CHARACTER,"-dim",dim,flg,ierr);CHKERRQ(ierr)


   call DMPlexCreateBoxMesh(PETSC_COMM_WORLD,dim,PETSC_TRUE,seqDM,ierr);CHKERRQ(ierr)
   call DMGetDimension(seqDM,dim,ierr);CHKERRQ(ierr)


   allocate(numDof(1));numDof = dim
   allocate(numComp(1));numComp = [2]   
   allocate(numDof(3)); numDof = [2,0,0] !{Vertex,Edge,Cell}
   allocate(bcFields(1)); bcFields = [0]

   call DMPlexCreateSection(seqDM,dim,numFields,numComp,numDof,numBC,bcFields,bcPoints,PETSC_NULL_OBJECT,seqSection,ierr);CHKERRQ(ierr)
   call DMSetDefaultSection(seqDM,seqSection,ierr);CHKERRQ(ierr)
   call DMPlexDistribute(seqDM,0,pointSF,distDM,ierr);CHKERRQ(ierr)

   call DMGetDefaultSection(distDM,distSection,ierr);CHKERRQ(ierr)

   call DMPlexCreateGlobalToNaturalPetscSF(distDM,global2naturalSF,ierr);CHKERRQ(ierr)
   call DMPlexSetGlobalToNaturalPetscSF(distDM,global2naturalSF,ierr);CHKERRQ(ierr)

   call DMGetCoordinates(distDM,coord,ierr);CHKERRQ(ierr)
   call DMCreateGlobalVector(distDM,distV,ierr);CHKERRQ(ierr);
   call VecCopy(coord,distV,ierr);CHKERRQ(ierr);

   call PetscObjectSetName(distV,"distV",ierr);CHKERRQ(ierr)
   if (verbose) then
      call VecGetSize(distV,sz,ierr);CHKERRQ(ierr)
      call VecGetBlockSize(distV,bs,ierr);CHKERRQ(ierr)
      write(IOBuffer,*) "==== original V in global ordering. size==",sz,"block size=",bs,"\n"
      call PetscPrintf(PETSC_COMM_WORLD,IOBuffer,ierr);CHKERRQ(ierr)
      call VecView(distV,PETSC_VIEWER_STDOUT_WORLD,ierr);CHKERRQ(ierr)
   end if

   call DMCreateGlobalVector(distDM,natV,ierr);CHKERRQ(ierr)
   call PetscObjectSetName(natV,"natV",ierr);CHKERRQ(ierr)
   call DMPlexGlobalToNaturalBegin(distDM,distV,natV,ierr);CHKERRQ(ierr)
   call DMPlexGlobalToNaturalEnd(distDM,distV,natV,ierr);CHKERRQ(ierr)
   if (verbose) then
      call VecGetSize(natV,sz,ierr);CHKERRQ(ierr)
      call VecGetBlockSize(natV,bs,ierr);CHKERRQ(ierr)
      write(IOBuffer,*) "==== V in natural ordering. size==",sz,"block size=",bs,"\n"
      call PetscPrintf(PETSC_COMM_WORLD,IOBuffer,ierr);CHKERRQ(ierr)
      call VecView(natV,PETSC_VIEWER_STDOUT_WORLD,ierr);CHKERRQ(ierr)
   end if

   if (test_write) then
   call PetscViewerHDF5Open(PETSC_COMM_WORLD,"V.h5",FILE_MODE_write,hdf5Viewer,ierr);CHKERRQ(ierr)
   if (test_native) then
      call PetscViewerPushFormat(hdf5Viewer,PETSC_VIEWER_NATIVE,ierr);CHKERRQ(ierr)
   end if
   call VecView(distV,hdf5Viewer,ierr);CHKERRQ(ierr)
   call PetscViewerDestroy(hdf5Viewer,ierr);CHKERRQ(ierr)
   end if

   if (test_stdout .AND. test_native) then
      call PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_NATIVE);CHKERRQ(ierr);
      call VecView(distV,PETSC_VIEWER_STDOUT_WORLD,ierr);CHKERRQ(ierr);
   end if

   if (test_read) then
      call DMCreateGlobalVector(distDM,readV,ierr);CHKERRQ(ierr)
      call PetscObjectSetName(readV,"distV",ierr);CHKERRQ(ierr)
      call PetscViewerHDF5Open(PETSC_COMM_WORLD,"V.h5",FILE_MODE_READ,hdf5Viewer,ierr);CHKERRQ(ierr)
      if (test_native) then
         call PetscViewerPushFormat(hdf5Viewer,PETSC_VIEWER_NATIVE,ierr);CHKERRQ(ierr)
      end if
      call VecLoad(readV,hdf5Viewer,ierr);CHKERRQ(ierr)
      call PetscViewerDestroy(hdf5Viewer,ierr);CHKERRQ(ierr)
      if (verbose) then
         call VecGetSize(readV,sz,ierr);CHKERRQ(ierr)
         call VecGetBlockSize(readV,bs,ierr);CHKERRQ(ierr)
         write(IOBuffer,*) "==== Vector from file. size==",sz,"block size=",bs,"\n"
         call PetscPrintf(PETSC_COMM_WORLD,IOBuffer,ierr);CHKERRQ(ierr)
         call VecView(readV,PETSC_VIEWER_STDOUT_WORLD,ierr);CHKERRQ(ierr)
      end if

      if (test_native) then
         call VecEqual(readV,distV,flg,ierr);CHKERRQ(ierr)
         if (flg) then
            call PetscPrintf(PETSC_COMM_WORLD,"Vectors are equal\n",ierr);CHKERRQ(ierr)
         else
            call PetscPrintf(PETSC_COMM_WORLD,"Vectors are not equal\n",ierr);CHKERRQ(ierr)
            call VecAXPY(readV,-1.,distV,ierr);CHKERRQ(ierr)
            call VecNorm(readV,NORM_INFINITY,norm,ierr);CHKERRQ(ierr)
            write(IOBuffer,*) "diff norm is ",norm,"\n"
            call PetscPrintf(PETSC_COMM_WORLD,IOBuffer,ierr);CHKERRQ(ierr)
            call VecView(readV,PETSC_VIEWER_STDOUT_WORLD,ierr);CHKERRQ(ierr)
         end if
      end if
      call VecDestroy(readV,ierr);CHKERRQ(ierr)
   end if

   call VecDestroy(natV,ierr);CHKERRQ(ierr)
   call VecDestroy(distV,ierr);CHKERRQ(ierr)
   
   call DMDestroy(distDM,ierr);CHKERRQ(ierr)
   if (numproc > 1) then
      call DMDestroy(seqDM,ierr);CHKERRQ(ierr)
   end if
   call PetscFinalize(ierr)  
end program ex14f90
