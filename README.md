# Changes
## General
* DMPlexDistribute() now works for sequential case and returns a 1to1 pointSF.
* DMPlexDistribute() distributes the default Section.
## Global to Natural ordering
* DMPlexCreateGlobalToNaturalPetscSF() crates and DMPlexSetGlobalToNaturalPetscSF() sets the SF for mapping between global to natural and natural to global Vecs.
* G2N and N2G SFs get propagated when creating SubDMs
* PETSC_VIEWER_NATIVE for HDF5 Viewer now uses the G2N SF for saving and loading to and from Natural ordering, i.e., now one can save their Vecs on any number of cpus and load them on the different number of cpus.
 