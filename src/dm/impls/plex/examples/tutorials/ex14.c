static char help[] = "An example of writing a global Vec from a DMPlex with HDF5 format.\n";

#include <petscdmplex.h>
#include <petscsnes.h>
#include <petscsf.h>
#include <exodusII.h>
#include <petsc-private/dmimpl.h>
#include <petsc-private/vecimpl.h>
#include <petscsf.h>
#include <petscviewerhdf5.h>

#undef __FUNCT__
#define __FUNCT__ "main"
 int main(int argc,char **argv)
 {
  PetscErrorCode      ierr;
  DM                  dm,distDM;
  PetscBool           flg,test_native=PETSC_FALSE,test_write=PETSC_FALSE,test_read=PETSC_FALSE,test_stdout=PETSC_FALSE,verbose=PETSC_FALSE;
  PetscMPIInt         numproc,rank;
  PetscViewer         stdoutViewer,hdf5Viewer;
  PetscInt            dim=2,size,bs;
  PetscInt            numFields = 1;
  PetscInt            numComp[1] = {2};
  PetscInt            numDof[3] = {2,0,0};
  PetscInt            bcFields[1] = {0},numBC=0;
  IS                  bcPoints[1] = {NULL};
  PetscSection        seqSection,distSection;
  Vec                 coord,distV,natV,readV;
  PetscSF             pointSF,global2naturalSF;
  MPI_Comm            comm;
  PetscScalar         *coords;
  PetscScalar         norm;

  ierr = PetscInitialize(&argc,&argv,(char*)0,help);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;

  ierr = PetscOptionsBegin(PETSC_COMM_WORLD,"","Test Options","none");CHKERRQ(ierr);
  ierr = PetscOptionsBool("-test_native","Test writing in native format","",PETSC_FALSE,&test_native,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-test_write","Test writing to the HDF5 file","",PETSC_FALSE,&test_write,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-test_read","Test reading from the HDF5 file","",PETSC_FALSE,&test_read,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-test_stdout","Test stdout","",PETSC_FALSE,&test_stdout,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-verbose","print the Vecs","",PETSC_FALSE,&verbose,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-dim","the dimension of the problem","",2,&dim,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();

  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&numproc);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  ierr = DMPlexCreateBoxMesh(comm,dim,PETSC_TRUE,&dm);CHKERRQ(ierr);
  numDof[0]=dim;
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMPlexCreateSection(dm,dim,numFields,numComp,numDof,numBC,bcFields,bcPoints,NULL,&seqSection);CHKERRQ(ierr);
  ierr = DMSetDefaultSection(dm,seqSection);CHKERRQ(ierr);

  ierr = DMPlexDistribute(dm,0,&pointSF,&distDM);CHKERRQ(ierr);
  ierr = DMGetDefaultSection(distDM,&distSection);CHKERRQ(ierr);
  ierr = DMPlexCreateGlobalToNaturalPetscSF(distDM,&global2naturalSF);CHKERRQ(ierr);
  ierr = DMPlexSetGlobalToNaturalPetscSF(distDM,global2naturalSF);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(distDM,&distV);CHKERRQ(ierr);
  ierr = DMGetCoordinates(distDM,&coord);CHKERRQ(ierr);
  ierr = VecGetArray(coord,&coords);CHKERRQ(ierr);
  ierr = VecCopy(coord,distV);CHKERRQ(ierr);

  ierr = PetscObjectSetName((PetscObject) distV,"distV");CHKERRQ(ierr);
  if (verbose) {
    ierr = VecGetSize(distV,&size);CHKERRQ(ierr);
    ierr = VecGetBlockSize(distV,&bs);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD,"==== original V in global ordering. size==%d\tblock size=%d\n",size,bs);CHKERRQ(ierr);
    ierr = VecView(distV,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }

  ierr = DMCreateGlobalVector(distDM,&natV);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) natV,"natV");CHKERRQ(ierr);
  ierr = DMPlexGlobalToNaturalBegin(distDM,distV,natV);CHKERRQ(ierr);
  ierr = DMPlexGlobalToNaturalEnd(distDM,distV,natV);CHKERRQ(ierr);
  if (verbose) {
    ierr = VecGetSize(natV,&size);CHKERRQ(ierr);
    ierr = VecGetBlockSize(natV,&bs);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD,"====  V in natural ordering. size==%d\tblock size=%d\n",size,bs);CHKERRQ(ierr);
    ierr = VecView(natV,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }

  if (test_write) {
    ierr = PetscViewerHDF5Open(comm,"V.h5",FILE_MODE_WRITE,&hdf5Viewer);CHKERRQ(ierr);
    if (test_native) {
      ierr = PetscViewerPushFormat(hdf5Viewer,PETSC_VIEWER_NATIVE);CHKERRQ(ierr);
    }
    ierr = VecView(distV,hdf5Viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&hdf5Viewer);CHKERRQ(ierr);
  }

  if (test_stdout && test_native) {
    ierr = PetscViewerASCIIGetStdout(comm,&stdoutViewer);CHKERRQ(ierr);
    ierr = PetscViewerPushFormat(stdoutViewer,PETSC_VIEWER_NATIVE);CHKERRQ(ierr);
    ierr = VecView(distV,stdoutViewer);CHKERRQ(ierr);
  }

  if (test_read) {
    ierr = DMCreateGlobalVector(distDM,&readV);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject) readV,"distV");CHKERRQ(ierr);
    ierr = PetscViewerHDF5Open(comm,"V.h5",FILE_MODE_READ,&hdf5Viewer);CHKERRQ(ierr);
    if (test_native) { 
      ierr = PetscViewerPushFormat(hdf5Viewer,PETSC_VIEWER_NATIVE);CHKERRQ(ierr);
    }
    ierr = VecLoad(readV,hdf5Viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&hdf5Viewer);CHKERRQ(ierr);
    if (verbose) {
      ierr = VecGetSize(readV,&size);CHKERRQ(ierr);
      ierr = VecGetBlockSize(readV,&bs);CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"==== Vector from file. size==%d\tblock size=%d\n",size,bs);CHKERRQ(ierr);
    }
    if (test_native) {
      ierr = VecEqual(readV,distV,&flg);CHKERRQ(ierr);
      if (flg) {
        ierr = PetscPrintf(PETSC_COMM_WORLD,"Vectors are equal\n");CHKERRQ(ierr);
      } else {
        ierr = PetscPrintf(PETSC_COMM_WORLD,"\nVectors are not equal\n\n");CHKERRQ(ierr);
        ierr = VecAXPY(readV,-1.,distV);CHKERRQ(ierr);
        ierr = VecNorm(readV,NORM_INFINITY,&norm);CHKERRQ(ierr);
        ierr = PetscPrintf(PETSC_COMM_WORLD,"diff norm is = %f",norm);CHKERRQ(ierr);
        ierr = VecView(readV,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
      }
      ierr = VecDestroy(&readV);CHKERRQ(ierr);
    }
  }
  ierr = VecRestoreArray(coord,&coords);CHKERRQ(ierr);
  ierr = VecDestroy(&distV);CHKERRQ(ierr);
  ierr = VecDestroy(&natV);CHKERRQ(ierr);

  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  if (numproc!=1) {
    ierr = DMDestroy(&distDM);CHKERRQ(ierr);
  }
  ierr = PetscFinalize();
  return 0;
  }